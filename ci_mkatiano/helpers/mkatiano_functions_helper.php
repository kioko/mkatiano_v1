<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('SALT', $this->config->item('SALT'));
define('CB_KEY', $this->config->item('CB_KEY'));

//Functions for the eKanjo System done on HMVC + CouchBase

/**
 * Create a new user account 
 * 1 - Check if mail is in use first.
 * 2 - Return error or add if inexistent.
 * 3 - Compose and queue email for user.
 *
 * @access	public
 * @param       STRING $uEmail Email address
 * @param       STRING $uNames Full names of the user
 * @param       STRING $uGender Gender of the person
 * @return	string
 */
function mkatiano_DB_userAdd($uEmail, $uNames, $uGender) {
    $response = "";
    $CI =& get_instance();
    
    //Check Existense
    $whereData = array('u_email' => $uEmail);
    $CI->db->where($whereData);
    $CI->db->select('pkid, u_fullnames');
    $CI->db->limit(1);
    $query = $CI->db->get('mk_users');

    if ($query->num_rows() > 0){ // User with that email exists
        
        $username = "";
        foreach ($query->result() as $row){
            $username = $row->u_fullnames;
        }
        $response = '{"status":"0","txt":"The email address already has an account under the name : '.$username.'. Kindly login if that is you"}';
    } else { // User does not exist
    
        // 1a - Add user
        $randPass = random_string();
        $encPass = mkatiano_STR_encrypt($randPass);
        
        $CI->db->trans_start();
            $insertData = array(
               'u_email' =>  $uEmail,
               'u_password' => $encPass,
               'u_fullnames' => $uNames,
               'u_gender' => $uGender
            );
            $CI->db->insert('mk_users', $insertData);
            
            $insID = $CI->db->insert_id();
        
        //1b - Add default preferences
            $insertData = array(
               'user_id' =>  $insID,
               'up_emailonlike' => 1,
               'up_smsonlike' => 1
            );
            $CI->db->insert('mk_userpreferences', $insertData);
        $CI->db->trans_complete();
        
        // 2 - Queue email
        $emailMsg = "Hello ".$uNames."<br/><br/>";
        $emailMsg .= "Welcome to Mkatiano. Your temporary password is : ".$randPass."<br/><br/>";
        $emailMsg .= "Please login and change this to a password you are more comfortable with.<br/><br/>";
        $emailMsg .= "Jibambe mzito.<br/><br/>";
        
        $eSubject = "Welcome to Mkatiano";
        
        mkatiano_DB_saveEmail($uEmail, $emailMsg, $eSubject);
        
        //Delete Cache Keys
        $cachekey = md5(CB_KEY.'mkatiano_DB_getUnsentEmails');
        mkatiano_CB_deleteData($cachekey);
        
        $response = '{"status":"1","txt":"Account created. Please check your email for your password. Good day!"}';
        
    }
    
    return $response;
}


/**
 * Reset a user password  
 * 1 - Create new password.
 * 2 - Email new password to user.
 *
 * @access	public
 * @param      STRING $uEmail Email address
 * @return	string
 */
function mkatiano_DB_userResetPass($uEmail, $uNames) {
    $response = "";
    $CI =& get_instance();
    
    //Check Existense
    $whereData = array('u_email' => $uEmail);
    $CI->db->where($whereData);
    $CI->db->select('pkid, u_fullnames');
    $CI->db->limit(1);
    $query = $CI->db->get('mk_users');

    if ($query->num_rows() > 0){ // Email address exists
        // 1a - Generate random password
        $randPass = random_string();
        $encPass = mkatiano_STR_encrypt($randPass);

        // 1b - Update password and compose email to user 
        $CI->db->trans_start();
            $CI->db->where($whereData);
            $CI->db->set('u_password', $encPass)->update ('mk_users');
        $CI->db->trans_complete();
        
        // 2 - Queue email
        $emailMsg = "Hello ".$uNames."<br/><br/>";
        $emailMsg .= "Your Mkatiano password has been reset. Your temporary password is : ".$randPass.".<br/><br/>";
        $emailMsg .= "Please login and change this to a password you are more comfortable with.<br/><br/>";
        $emailMsg .= "Jibambe mzito.<br/><br/>";
        
        $eSubject = "Mkatiano password reset";
        
        mkatiano_DB_saveEmail($uEmail, $emailMsg, $eSubject);
        
        //Delete Cache Keys
        $cachekey = md5(CB_KEY.'mkatiano_DB_getUnsentEmails');
        mkatiano_CB_deleteData($cachekey);
        
        $response = '{"status":"1","txt":"Password reset okay. Please check your email for your password. Good day!"}';
        
    } else { // User does not exist
        $response = '{"status":"0","txt":"The email address provided is not registered. Please check for typos, or register this account!"}';
        
    }
    
    return $response;
}


/**
 * Reset a user password  
 * 1 - Create new password.
 * 2 - Email new password to user.
 *
 * @access	public
 * @param      STRING $uEmail Email address
 * @return	string
 */
function mkatiano_DB_passwordChange($uPKID, $oldPass, $newPass, $uNames) {
    $response = "";
    $CI =& get_instance();
    
    //Check old password
    $whereData = array('u_password' => mkatiano_STR_encrypt($oldPass) );
    $CI->db->where($whereData);
    $CI->db->select('pkid, u_email');
    $CI->db->limit(1);
    $query = $CI->db->get('mk_users');

    if ($query->num_rows() > 0){ // Old password is OK 

        // 1b - Update password and compose email to user 
        $CI->db->trans_start();
            $CI->db->where($whereData);
            $CI->db->set('u_password', mkatiano_STR_encrypt($newPass))->update ('mk_users');
        $CI->db->trans_complete();
        
        // 2 - Queue email
        $emailMsg = "Hello ".$uNames."<br/><br/>";
        $emailMsg .= "Your Mkatiano password has been changed to the one you specified.<br/><br/>";
        $emailMsg .= "Kindly use the new password you specified for future logins.<br/><br/>";
        $emailMsg .= "Jibambe mzito.<br/><br/>";
        
        foreach ($query->result() as $row){ $uEmail = $row->u_email; }
        
        $eSubject = "Mkatiano password changed";
        
        mkatiano_DB_saveEmail($uEmail, $emailMsg, $eSubject);
        
        //Delete Cache Keys
        $cachekey = md5(CB_KEY.'mkatiano_DB_getUnsentEmails');
        mkatiano_CB_deleteData($cachekey);
        
        $response = '{"status":"1","txt":"Password changed okay"}';
        
    } else { // User does not exist
        $response = '{"status":"0","txt":"The OLD password you specified is incorrect!"}';
        
    }
    
    return $response;
}


/**
 * Log in an existing user 
 * 1 - Check if mail and password are correct .
 * 2 - Return error if wrong or login existent.
 *
 * @access	public
 * @param       STRING $uEmail Email address
 * @param       STRING $uPass Supplied password
 * @return	string
 */
function mkatiano_DB_userLogin($uEmail, $uPass) {
    $response = "";
    $CI =& get_instance();
    
    //Check Existense
    $whereData = array(
        'u_email' => $uEmail,
        'u_password' => mkatiano_STR_encrypt($uPass)
        );
    $CI->db->where($whereData);
    $CI->db->select('pkid, u_fullnames, u_gender, u_active, u_lastlogin');
    $CI->db->limit(1);
    $query = $CI->db->get('mk_users');

    if ($query->num_rows() > 0)
    {
        foreach ($query->result() as $row)
        {
           // Data Found but user inActive - Return 'INACTIVE'
           if ($row->u_active=="0") {
                $response = '{"status":"0","txt":"Login was OK, but yor account is inactive. Please check your email for an activation link."}';
           } else {
                // All OK - Set Cookie, Increment Login Count and return 'OK'
                $sessData = array(
                   'name'           => $row->u_fullnames,
                   'pkid'           => $row->pkid,
                   'email'          => $uEmail,
                   'gender'         => $row->u_gender,
                   'lastlogin'      => $row->u_lastlogin,
                   'logged_in'      => TRUE
                );
                $CI->session->set_userdata($sessData);
                
                //Update last login and logn counts
                $whereArray = array('pkid' => $row->pkid);
                $CI->db->where($whereArray);
                $CI->db->set('u_lastlogin', date('Y-m-d H:i:s'))->set('u_logincount', 'u_logincount + 1', FALSE)->update ('mk_users');
 
                $response = '{"status":"1","txt":""}';   
            }
        }
    } else {
        // No valid accounts - Return 'FAIL'
        $response = '{"status":"0","txt":"The login credentials supplied are incorrect. Please try again."}';
    }
    
    return $response;
}


/**
 * Update the read state of an email 
 *
 * @access	public
 * @param       INTEGER $e_pkid Email PKID 
 * @param       INTEGER $e_state Email State
 * @return	string
 */
function mkatiano_DB_updateEmailState($e_pkid, $e_state) {
    $response = "";
    $CI =& get_instance();

    //Update email state 
    $whereArray = array('pkid' => $e_pkid);
    $CI->db->where($whereArray);
    $CI->db->set('e_sent',$e_state)->set('e_sendtime', date('Y-m-d H:i:s'))->update ('mk_emails');

    $response = '{"status":"1","txt":""}';   
    
    return $response;
}


/**
 * Save an email in the emails table. These emails are sent by a seperate process 
 *
 * @access	public
 * @param       STRING $eEmail Email address
 * @param       STRING $eMessage Email to be sent 
 * @return	string
 */
function mkatiano_DB_saveEmail($eEmail, $eMessage, $eSubject) {
    $response = "";
    $CI =& get_instance();
    
        $CI->db->trans_start();
            $insertData = array(
               'e_recepient' => $eEmail,
               'e_message' => $eMessage,
               'e_subject' => $eSubject
            );
            $CI->db->insert('mk_emails', $insertData);
        $CI->db->trans_complete();

}


/**
 * Get all unsent email from the DB 
 *
 * @access	public
 * @param       NONE
 * @return	ARRAY
 */
function mkatiano_DB_getUnsentEmails() {
    $response = "";
    $CI =& get_instance();
    $cachekey = md5(CB_KEY.'mkatiano_DB_getUnsentEmails');
    
    //Check Cache Existense
    $query = mkatiano_CB_getData($cachekey);
    
        if (!$query) {
            echo "<font color=red>cache bilas [$cachekey]...<br/></font>";
            $whereData = array('e_sent' => 0);
            $CI->db->where($whereData);
            $CI->db->select('pkid, e_recepient, e_message, e_subject', FALSE);
            $CI->db->limit(10);
            $query = $CI->db->get('mk_emails');

            $response = $query->result();

            //Write to Cache
            if (sizeof($response)==0) { 
                mkatiano_CB_setData($cachekey, "_empty_");
            }else {
                mkatiano_CB_setData($cachekey, $response);
            }
            
        } else {
            echo "<font color=blue>cache iko  [$cachekey]...<br/></font>";
            $response = $query;
        }
        
    return $response;

}


/**
 * Flush all the data from CouchBase and clear the data bucket
 *
 * @access	public
 * @param NONE $none No parameters
 * @return	string
 */
function mkatiano_CB_flushData() {
    // Initialize the CouchBase Handle:
    if (!$cb_handle) {$cb_handle = new Couchbase("127.0.0.1:8091", "Administrator", "Salimix0", "cb_buck_MKATIANO");}
    
    //$cb_handle = Couchbasesingleton::getInstance()->getCouchbase() or die ("Err:". $cb_handle->getResultMessage() );
    return $cb_handle->flush();
}


/**
 * Add a new item to the Couchbase Cache
 *
 * @access	public
 * @param      String $dataKey      The key of the cache item
 * @param      String $dataValue    The value of the cache item
 * @return	None
 */
function mkatiano_CB_setData($dataKey, $dataValue, $dataExpiry=600) {
    // Initialize the CouchBase Handle:
    if (!$cb_handle) {$cb_handle = new Couchbase("127.0.0.1:8091", "Administrator", "Salimix0", "cb_buck_MKATIANO");}
    return $cb_handle->set($dataKey, $dataValue, $dataExpiry);
}


/**
 * Fetch an item from the Couchbase Cache
 *
 * @access	public
 * @param      String $dataKey      The key of the cache item
 * @return	String
 */
function mkatiano_CB_getData($dataKey) {
    // Call the global the CouchBase Handle:
    if (!$cb_handle) {$cb_handle = new Couchbase("127.0.0.1:8091", "Administrator", "Salimix0", "cb_buck_MKATIANO");}
    return $cb_handle->get($dataKey);
}


/**
 * Delete an item from the Couchbase Cache
 *
 * @access	public
 * @param      String $dataKey      The key of the cache item
 * @return	None
 */
function mkatiano_CB_deleteData($dataKey) {
    // Initialize the CouchBase Handle:
    if (!$cb_handle) {$cb_handle = new Couchbase("127.0.0.1:8091", "Administrator", "Salimix0", "cb_buck_MKATIANO");}
    return $cb_handle->delete($dataKey);
}


/**
 * Encrypt a string using SALT
 *
 * @access	public
 * @param	string $text The text to be encrypted
 * @return	string
 */

function mkatiano_STR_encrypt($text) { 
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB)));
} 


/**
 * Decrypt a string using SALT
 *
 * @access	public
 * @param	string $text The text to be decrypted
 * @return	string
 */
function mkatiano_STR_decrypt($text) { 
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB));
} 


/**
 * Send email to a reecpient
 * If success, return OK;
 * If fail, return FAIL;
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	string
 * @return	string
 */
function mkatiano_NET_sendEmail($to, $subject, $message)
{
    $CI =& get_instance();
    
    $CI->email->from('mkatiano_registrations-dont-reply@xema.mobi', 'Mkatiano 1.0');
    $CI->email->to($to); 

    $CI->email->subject($subject);
    $CI->email->message($message);	

    return $CI->email->send();

    //echo $CI->email->print_debugger();    
    
}


/**
 * Get real IP of user
 * If fail, return 127.0.0.1;
 * If success, then return 'Real IP';
 *
 * @access	public
 * @return	string
 */
function mkatiano_NET__getIPAddress() {

    if (isSet($_SERVER)) {
        if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } elseif (isSet($_SERVER["HTTP_CLIENT_IP"])) {
            $realip = $_SERVER["HTTP_CLIENT_IP"];
        } else {
            $realip = $_SERVER["REMOTE_ADDR"];
        }

    } else {

        if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
            $realip = getenv( 'HTTP_X_FORWARDED_FOR' );
        } elseif ( getenv( 'HTTP_CLIENT_IP' ) ) {
            $realip = getenv( 'HTTP_CLIENT_IP' );
        } else {
            $realip = getenv( 'REMOTE_ADDR' );
        }
    }
    return $realip;

}

/**
 * Show the time to upto 3 milliseconds long
 *
 * @access	public
 * @param	timestamp
 * @return	string
 */
function mkatiano_MISC_toTimestamp(){
    
    $timeparts = explode(" ",microtime());
    $milliseconds = bcadd(($timeparts[0]*1000),bcmul($timeparts[1],1000));
 
    $seconds = $milliseconds / 1000;
    $remainder = round($seconds - ($seconds >> 0), 3) * 1000;

    return date('Y-m-d H:i:s.', $seconds).$remainder;
}

?>
