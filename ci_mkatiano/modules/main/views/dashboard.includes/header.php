<div id="header">
    
    <div id="top">
        <div class="left">
            <p>Welcome, <strong><?=$username;?></strong> [ <a href="/logout">logout</a> ]</p>
        </div>
        <div class="right">
            <div class="align-right">
                <p>Last login : <strong><?=date("jS M H:i",strtotime($lastlogin));?>hrs</strong></p>
            </div>
        </div>
    </div>

    <div id="nav">
        <ul>
            <li class="upp"><a href="#">Profile</a>
                <ul>
                    <li>&#8250; <a href="/">My Timeline</a></li>
                    <li>&#8250; <a href="/profile">My Profile</a></li>
                    <li>&#8250; <a href="/friends">My Friends</a></li>
                </ul>
            </li>

            <li class="upp"><a href="#">My Media</a>
                <ul>
                    <li>&#8250; <a href="/photos">Photos</a></li>
                    <li>&#8250; <a href="/videos">Videos</a></li>
                </ul>
            </li>

            <li class="upp"><a href="#">Settings</a>
                <ul>
                    <li>&#8250; <a href="/passwordchange">My password</a></li>
                    <li>&#8250; <a href="/preferences">Account preferences</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
