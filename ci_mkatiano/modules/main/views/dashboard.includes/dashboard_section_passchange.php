
<div class="full_w">
    <div class="h_title">Preferences</div>
    <h1>Change your mkatiano password</h1>
    <p>In the form below, enter your current password, then type and confirm your new password.</p>

        <form action="" method="post" id="passChangeForm">
                <div class="element">
                    <label for="cPass">Current password</label>
                    <input id="cPass" name="cPass" class="textnusu" type="password"/>
                </div>
            
                <div class="element">
                    <label for="cPass">New password</label>
                    <input id="nPass" name="nPass" class="textnusu" type="password"/>
                </div>
            
                <div class="element">
                    <label for="cPass">Confirm new password</label>
                    <input id="nPassC" name="nPassC" class="textnusu" type="password"/>
                </div>

                <div class="entry">
                    <button type="submit" class="add">Change password</button> - &nbsp; <a class="button cancel" href="/">Cancel</a>
                </div>
        </form>
    
 <!--
    <div class="n_warning"><p>Attention notification. Lorem ipsum dolor sit amet, consetetur, sed diam nonumyeirmod tempor.</p></div>
    <div class="n_ok"><p>Success notification. Lorem ipsum dolor sit amet, consetetur, sed diam nonumyeirmod tempor.</p></div>
    <div class="n_error"><p>Error notification. Lorem ipsum dolor sit amet, consetetur, sed diam nonumyeirmod tempor.</p></div>
 -->
 
</div>
