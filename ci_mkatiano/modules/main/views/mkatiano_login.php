
<!-- Libraries -->
    <? include 'ci_mkatiano/helpers/mk_header.php'; ?>
    <? include 'ci_mkatiano/helpers/login_js_css.php'; ?>
<!-- End of Libraries -->

</head>
    <body>
    <div class="wrap">
        <div id="content">
            <div id="main">
                <div class="full_w">
                    <p class="descr">
                        Learn HMVC PHP Coding the right way. Blazing speed apps with the latest and most stable combination of web/server technologies.
                        Commented, manageable code with separation of business logic, display and aesthetics.
                    </p>
                    
                    <form action="" method="post" id="loginForm">
                        <label for="mk_uemail">Email:</label>
                        <input id="mk_uemail" name="mk_uemail" class="text" />
                        <label for="mk_pass">Password:</label>
                        <input id="mk_pass" name="mk_pass" type="password" class="text" />
                        <div class="sep"></div>
                        
                        <center>
                            <button type="submit" class="ok">Login</button> | &nbsp;&nbsp; <a class="button add" href="/join">Create account</a>
                            <br/> <br/>
                            <a class="button forgot" href="/lostpassword">Forgotten password?</a>
                        </center>
                            
                    </form>
                </div>
                <div class="footer">&raquo; <a href="http://www.iddsalim.com/">By Idd Salim</a> </div>
            </div>
        </div>
    </div>
    </body>
</html>
