<?php


/**
 * Description of Mailsender
 *
 * @author iddsalim
 */
class Mailsender extends MX_Controller{
    //put your code here
    
    public function mailsender(){
        parent::__construct();
    }
    
    public function send_emails(){
        echo mkatiano_MISC_toTimestamp()." :: Initializing email sender<br/> ";
        
        //Get all emails from the DB
        $allMails = mkatiano_DB_getUnsentEmails();
        $cachekey = md5($this->config->item('CB_KEY').'mkatiano_DB_getUnsentEmails');
        
        //Check cache for empty stub and populate size variable
        $cacheSize =(sizeof($allMails) > 0 && ($allMails <>"_empty_") ) ? sizeof($allMails) : 0;
        
        if ($cacheSize > 0 ) {
            //Send each and mark as sent (if $mailResp===1) on each send
            foreach ($allMails as $email) {
                $mailResp = mkatiano_NET_sendEmail($email->e_recepient,$email->e_subject,$email->e_message );
                if ($mailResp==1) {
                    //Mark mail as sent in DB
                    mkatiano_DB_updateEmailState($email->pkid, 1);
                } 
            }

            //Invalidate CacheKey
            mkatiano_CB_deleteData($cachekey);
        }
        
        echo mkatiano_MISC_toTimestamp()." :: ".$cacheSize." emails sent. Concluding email sender<br/> ";
        
    }
}

?>
